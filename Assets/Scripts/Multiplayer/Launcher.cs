using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Launcher : MonoBehaviourPunCallbacks
{
    public static Launcher Instance;
    public static string mapSelection;
    
    [SerializeField] TMP_Text errorText, roomNameText;
    [SerializeField] Transform roomListContent, playerListContent;
    [SerializeField] GameObject roomListItemPrefab, playerListItemPrefab, loadingIndicator;
    [SerializeField] AudioSource song;
    [SerializeField] Button joinButton, startButton, startGameButton;
    [SerializeField] TMP_Dropdown mapDropdown;
    
    void Awake() {
        Instance = this;
    }

    void Start() {
        PhotonNetwork.ConnectUsingSettings();
        song.Play();
    }
    
    public void QuitGame() {
        Application.Quit(0);
    }
    
    public override void OnConnectedToMaster() {
        Debug.Log("Connected to master server");
        PhotonNetwork.JoinLobby();
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    public override void OnJoinedLobby() {
        joinButton.interactable = true;
        startButton.interactable = true;
        loadingIndicator.SetActive(false);
        Debug.Log("Joined Lobby");
    }

    public void CreateRoom() {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 10;
        PhotonNetwork.CreateRoom(PhotonNetwork.NickName, roomOptions);
        loadingIndicator.SetActive(true);
    }

    
    public override void OnJoinedRoom() {
        MenuManager.Instance.OpenMenu("room");
        loadingIndicator.SetActive(false);
        roomNameText.text = PhotonNetwork.CurrentRoom.Name;
        
        
        Player[] players = PhotonNetwork.PlayerList;

        foreach (Transform child in playerListContent) {
            Destroy(child.gameObject);
        }
        
        for (int i = 0; i < players.Length; i++){
            Instantiate(playerListItemPrefab, playerListContent).GetComponent<PlayerListItem>().SetUp(players[i]);
        }

        if (PhotonNetwork.IsMasterClient){
            startGameButton.interactable = true;
            mapDropdown.interactable = true;
        }
        else{
            startGameButton.interactable = false;
            mapDropdown.interactable = false;
        }
    }
    
    public override void OnMasterClientSwitched(Player newMasterClient) {
        if (PhotonNetwork.IsMasterClient){
            startGameButton.interactable = true;
            mapDropdown.interactable = true;
        }
        else{
            startGameButton.interactable = false;
            mapDropdown.interactable = false;
        }
    }
    
    public override void OnCreateRoomFailed(short returnCode, string message) {
        errorText.text = "Room creation failed: " + message;
        MenuManager.Instance.OpenMenu("error");
    }

    public void StartGame() {
        PhotonNetwork.LoadLevel("Loading");
    }
    public void LeaveRoom() {
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.JoinLobby();
        loadingIndicator.SetActive(true);
    }
    
    public void JoinRoom(RoomInfo info) {
        PhotonNetwork.JoinRoom(info.Name);
        loadingIndicator.SetActive(true);
    }
    
    public override void OnLeftRoom() {
        MenuManager.Instance.OpenMenu("title");
        loadingIndicator.SetActive(false);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList) {
        foreach (Transform trans in roomListContent){
            Destroy(trans.gameObject);
        }
        for (int i = 0; i < roomList.Count; i++){
            if(roomList[i].RemovedFromList){
                continue;
            }
            Instantiate(roomListItemPrefab, roomListContent).GetComponent<RoomListItem>().SetUp(roomList[i]);
        }
    }
    
    public override void OnPlayerEnteredRoom(Player newPlayer) {
        Instantiate(playerListItemPrefab, playerListContent).GetComponent<PlayerListItem>().SetUp(newPlayer);
    }
    
    public void OnDropdownValueChanged() { 
        mapSelection = mapDropdown.options[mapDropdown.value].text;
    }
}
