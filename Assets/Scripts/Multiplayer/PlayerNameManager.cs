using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using TMPro;
using Random = UnityEngine.Random;

public class PlayerNameManager : MonoBehaviour
{
    [SerializeField] TMP_InputField usernameText;

    void Start() {
        if (PlayerPrefs.HasKey("username")){
            usernameText.text = PlayerPrefs.GetString("username");
            PhotonNetwork.NickName = PlayerPrefs.GetString("username");
        }
        else{
            usernameText.text = "Simulator " + Random.Range(0, 1000).ToString("0000");
            OnUsernameInputValueChanged();
        }
    }

    public void OnUsernameInputValueChanged()
    {
        if (usernameText.text != "" && usernameText.text.Length <= 40){
            PhotonNetwork.NickName = usernameText.text;
            PlayerPrefs.SetString("username", usernameText.text);
        }
    }
}
