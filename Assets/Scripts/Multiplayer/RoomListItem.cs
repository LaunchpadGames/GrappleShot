using System.Collections;
using System.Collections.Generic;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RoomListItem : MonoBehaviour
{
    [SerializeField] TMP_Text name;
    [SerializeField] TMP_Text mode;
    [SerializeField] TMP_Text count;
    [SerializeField] Button playButton;
    public RoomInfo info;
    
    public void SetUp(RoomInfo _info) {
        info = _info;
        name.text = info.Name;
        mode.text = "DM";
        count.text = info.PlayerCount + " / " + info.MaxPlayers;
        
        if(info.PlayerCount == info.MaxPlayers) {
            playButton.interactable = false;
        }
    }
    
    public void OnClick() {
        Launcher.Instance.JoinRoom(info);
    }
}
