using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LauncherScript : MonoBehaviour
{
    public Rigidbody objectRigidBody;
    [Range(0.5f, 10f)]
    public float launcherStrength;
    
    private void OnTriggerEnter(Collider other)
    {
        objectRigidBody = other.gameObject.GetComponent<Rigidbody>();
        if (objectRigidBody == null) { }else{
            objectRigidBody.AddForce(transform.forward * launcherStrength * 1000);
        }
    }
}
