using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunRecoilHandler : MonoBehaviour
{
    public float revertSpeed;
    public float maxRecoil;
    
    void Update()
    {
        Quaternion zero = new Quaternion(0, 0, 0, 1);
        transform.localRotation = Quaternion.Lerp(transform.localRotation, zero, revertSpeed * Time.deltaTime);
    }
    public void AddRecoil(float amount)
    {
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x - amount, Random.Range(-amount/2, amount/2), transform.localEulerAngles.z);
        
    }
}
