using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GunController : Gun
{
    [Header("Gun Config")]

    [SerializeField] Camera cam;
    [SerializeField] Animator animator;

    [Header("UI Config")]
    public TextMeshProUGUI ammoCounter;
    public GameObject crosshair;
    public CrosshairUI crosshairManager;
    [Header("Visual Config")]
    public ParticleSystem muzzleFlash;

    bool canShoot;
    bool isReloading;
    bool isADS;
    bool canReload;

    private GunRecoilHandler gunRecoil;
    float maxAmmo;
    public float currentAmmo;
    float spreadAmountX, spreadAmountY;
    public PlayerController playerController;
    public ScopedSensEnabler scopedSensEnabler;
    public GameObject hitmarker;
    public Rigidbody rb;
    [Header("Networking Config")]
    PhotonView PV;

    void Awake() {
        PV = GetComponent<PhotonView>();
        canShoot = true;
        canReload = true;
        gunRecoil = transform.GetChild(0).GetComponent<GunRecoilHandler>();
        maxAmmo = (((GunInfo)itemInfo).maxAmmo);
        currentAmmo = maxAmmo;
    }

    public override void Use() {
        if (currentAmmo > 0 && !isReloading) {
            Shoot();
            if (isADS){
                animator.SetBool("ads", false);
                isADS = false;
                if(itemGameObject.activeSelf == true)
                {
                    crosshair.SetActive(true);
                }
            }
        }
    }


    void Update() {
        if (Input.GetMouseButton(1) && ((GunInfo)itemInfo).canADS && !isReloading) {
            animator.SetBool("ads", true);
            isADS = true;
            playerController.canSwitch = false;
            scopedSensEnabler.scoped = true;
            if(itemGameObject.activeSelf == true)
            {
                crosshair.SetActive(false);
            }
        }
        else if(((GunInfo)itemInfo).canADS) {
            animator.SetBool("ads", false);
            isADS = false;
            playerController.canSwitch = true;
            scopedSensEnabler.scoped = false;
            if (itemGameObject.activeSelf == true)
            {
                crosshair.SetActive(true);
            }
        }
        if (Input.GetKeyDown(KeyCode.R) && canReload && currentAmmo != maxAmmo && !isADS){ 
            Reload();
        }
        
        //auto reload if no bullets
        if (Input.GetMouseButtonDown(0) && currentAmmo <= 0){
            Reload();
        }
    }

    void Shoot() {
        if (canShoot) {
            crosshairManager.currentOffsetDistance += ((GunInfo)itemInfo).crosshairDisplaceAmount;
            
            PV.RPC("RPC_ShootCosmetic", RpcTarget.All);
            
            currentAmmo--;
            ammoCounter.text = currentAmmo.ToString();

            for (int i = 0; i < (((GunInfo)itemInfo).bullets); i++){
                Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f));
                if (!isADS){
                    spreadAmountX = (((GunInfo)itemInfo).bulletSpreadX);
                    spreadAmountY = (((GunInfo)itemInfo).bulletSpreadY);
                }
                else{
                    spreadAmountX = (((GunInfo)itemInfo).bulletSpreadADS);
                    spreadAmountY = (((GunInfo)itemInfo).bulletSpreadADS);
                }
                ray.direction = Quaternion.AngleAxis(Random.Range(-spreadAmountY,spreadAmountY), transform.up) * ray.direction;
                ray.direction = Quaternion.AngleAxis(Random.Range(-spreadAmountX,spreadAmountX), transform.right) * ray.direction;
                ray.origin = cam.transform.position;
                
                if (Physics.Raycast(ray, out RaycastHit hit))
                {
                    hit.collider.gameObject.GetComponent<IDamageable>()?.TakeDamage(((GunInfo)itemInfo).damage);
                    
                    if(hit.collider.gameObject.GetComponent<IDamageable>() != null)
                    {
                        StartCoroutine(ShowHitmarker());
                    }
                    
                    PV.RPC("RPC_Shoot", RpcTarget.All, hit.point, hit.normal);
                }
            }
        }
    }
    
    IEnumerator ShowHitmarker()
    {
        hitmarker.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        hitmarker.SetActive(false);
    }

    [PunRPC]
    void RPC_ShootCosmetic() {
        Instantiate(((GunInfo)itemInfo).shootSound);
        gunRecoil.AddRecoil(((GunInfo)itemInfo).visualGunRecoil);
        muzzleFlash.Play();
        if(((GunInfo)itemInfo).hasShootAnimation && !isADS)
        {
            animator.Play("shoot");
        }
        StartCoroutine(GunCooldown());
        if ((((GunInfo) itemInfo).boltAction)){
            StartCoroutine(BoltActionSound());
        }
        if (((GunInfo) itemInfo).knockback){
            // // Get the camera's transform component
            // Transform cameraTransform = Camera.main.transform;
            //
            // // Get the camera's current rotation
            // Quaternion cameraRotation = cameraTransform.rotation;
            //
            // // Negate the x, y, and z values of the rotation to get the opposite rotation
            // Quaternion oppositeRotation = new Quaternion(-cameraRotation.x, -cameraRotation.y, -cameraRotation.z, cameraRotation.w);
            //
            // rb.AddForce(oppositeRotation.x,oppositeRotation.y,oppositeRotation.z, ForceMode.Impulse);
            
            //help
        }
    }
    
    [PunRPC]
    void RPC_Shoot(Vector3 hitPosition, Vector3 hitNormal) {
        Collider[] colliders = Physics.OverlapSphere(hitPosition, 0.3f);
        if (colliders.Length != 0 && colliders[0].gameObject.layer != 6) {
            GameObject bulletImpactObj = Instantiate(bulletImpactPrefab, hitPosition + hitNormal * 0.001f, Quaternion.LookRotation(hitNormal, Vector3.up) * bulletImpactPrefab.transform.rotation);
            Destroy(bulletImpactObj, 10f);
            bulletImpactObj.transform.SetParent(colliders[0].transform);
        }
    }
    IEnumerator GunCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(((GunInfo)itemInfo).shotCooldown);
        canShoot = true;
    }

    IEnumerator BoltActionSound() {
        if (currentAmmo > 0){
            yield return new WaitForSeconds(0.3f);
            Instantiate(((GunInfo)itemInfo).newBulletSound);
        }
    }

    [PunRPC]
    void Reload() {
        Instantiate(((GunInfo)itemInfo).reloadSound);
        StartCoroutine(ReloadWait());
    }
    
    [PunRPC]
    IEnumerator ReloadWait() {
        canReload = false;
        isReloading = true;
        playerController.canSwitch = false;
        animator.SetBool("reloading",true);
        if(((GunInfo)itemInfo).shellLoading && currentAmmo == 0)
        {
            yield return new WaitForSeconds(((GunInfo)itemInfo).reloadTime + .25f);

        }
        else
        {
            yield return new WaitForSeconds(((GunInfo)itemInfo).reloadTime);

        }
        if (((GunInfo)itemInfo).boltAction && currentAmmo == 0){
            Instantiate(((GunInfo)itemInfo).newBulletSound);
        }
        //Set reloading to false
        isReloading = false;
        //For Shotguns only
        if (((GunInfo)itemInfo).shellLoading && currentAmmo < ((GunInfo)itemInfo).maxAmmo)
        {
            if (currentAmmo < ((GunInfo)itemInfo).maxAmmo - 1) 
            {
                
                Reload();
            }           
            else
            {
                animator.SetBool("reloading", false);
                canReload = true;
                playerController.canSwitch = true;
            }
            currentAmmo += 1;
        }
        else
        {
            currentAmmo = ((GunInfo)itemInfo).maxAmmo;

            animator.SetBool("reloading", false);
            canReload = true;
            playerController.canSwitch = true;
        }
        //Update ammo counter;
        ammoCounter.text = currentAmmo.ToString();
    }


}
