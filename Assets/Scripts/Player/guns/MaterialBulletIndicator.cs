using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialBulletIndicator : MonoBehaviour
{
    public Material coolMaterial;
    public Material hotMaterial;
    public GunController gun;
    public Material currentMaterial;
    private MeshRenderer mesh;
    // Start is called before the first frame update
    void Start()
    {
        //Get references
        mesh = gameObject.GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        currentMaterial.Lerp(coolMaterial, hotMaterial, gun.currentAmmo / ((GunInfo)gun.itemInfo).maxAmmo);
    }
}
