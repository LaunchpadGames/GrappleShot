using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CrosshairUI : MonoBehaviour
{
    public enum CrosshairStyle
    { 
            Assault_Rifle,
            Shotgun
    }

    [Header("Crosshair Configuration")]

    [Tooltip("Style of the crosshair, affects how many pieces move and in which direction.")]
    public CrosshairStyle crossHairStyle;

    [Tooltip("Original offset distance for the crosshair pieces, when not shooting pieces will revert to this distance.")]
    public float offsetDistance;

    [Tooltip("The speed at which the pieces revert back to their original places.")]
    public float revertSpeed;

    [Header("References")]

    public RectTransform[] crosshairPieces;

    [HideInInspector] public float currentOffsetDistance;
    
    void Update()
    {
        //Make pieces go back to their original spots
        currentOffsetDistance = Mathf.Lerp(currentOffsetDistance, offsetDistance, Time.deltaTime * revertSpeed);
        UpdateCrosshair();
    }
    public void UpdateCrosshair()
    {
        if (crossHairStyle == CrosshairStyle.Assault_Rifle)
        {
            crosshairPieces[0].localPosition = new Vector3(-currentOffsetDistance, crosshairPieces[0].localPosition.y, 0);
            crosshairPieces[1].localPosition = new Vector3(currentOffsetDistance, crosshairPieces[1].localPosition.y, 0);
        }
        else if (crossHairStyle == CrosshairStyle.Shotgun)
        {
            crosshairPieces[0].localPosition = new Vector3(-currentOffsetDistance, currentOffsetDistance, 0);
            crosshairPieces[1].localPosition = new Vector3(currentOffsetDistance, currentOffsetDistance, 0);
            crosshairPieces[2].localPosition = new Vector3(-currentOffsetDistance, -currentOffsetDistance, 0);
            crosshairPieces[3].localPosition = new Vector3(currentOffsetDistance, -currentOffsetDistance, 0);
        }
    }
}
