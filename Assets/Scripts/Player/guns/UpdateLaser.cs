using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateLaser : MonoBehaviour
{
    public LineRenderer laser;
    public Transform laserEntrance;
    public Transform laserExit;


    // Update is called once per frame
    void Update()
    {
        laser.SetPosition(0, laserEntrance.position);
        laser.SetPosition(1, laserExit.position);

    }
}
