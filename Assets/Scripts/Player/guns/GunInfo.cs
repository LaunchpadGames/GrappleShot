using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Items/New Gun")]
public class GunInfo : ItemInfo
{
    [Header("General Configuration")]

    [Tooltip("The damage done by a bullet fired by the gun. Note: This is not the total damage and for shotgun type guns this is the damage per pellet.")]
    public float damage;
    [Tooltip("Enables/Disables the ability to be able to hold the fire key to keep firing bullets. If disabled the player has to manually click to fire every shot.")]
    public bool isAutomatic;
    [Tooltip("The exact amount of time that it takes for a player to be able to fire another shot.")]
    public float shotCooldown;
    public bool boltAction;
    [Tooltip("Enables/Disables the ability for a gun to shoot multiple bullets. When enabled, the 'bullets' variable below controls how many bullets are fired.")]
    public bool shotgun;

    [Header("Bullet Configuration")]
    public float bullets;
    public float bulletSpreadX;
    public float bulletSpreadY;

    [Header("ADS Configuration")]

    [Tooltip("Enables/Disables the ability to aim down sights for a gun.")]
    public bool canADS;
    public float bulletSpreadADS = 0.05f;
    public bool sniperADS;

    [Header("Knockback Configuration")]

    [Tooltip("Enables/Disables knockback for a gun.")]
    public bool knockback;
    [Tooltip("The amount of knockback, or kickback applied to the player when the gun is fired. Note: This moves the player and is NOT visual only.")]
    public float knockbackForce;

    [Header("Ammo Configuration")]

    [Tooltip("The total ammo in one fully loaded clip.")]
    public int maxAmmo;
    [Tooltip("The time it takes for the player to reload. Keep in mind for shell loading weapons this is the amount of time it takes to load one shell.")]
    public float reloadTime;
    [Tooltip("Enables/Disables whether the gun reloads 1 bullet at a time. Note: Shell loading requires 3 animations instead of one.")]
    public bool shellLoading;

    [Header("Visual Configuration")]

    [Tooltip("The intensity of recoil the gun experiences when you fire it. Note: This is exclusively visual and only effects the model.")]
    public float visualGunRecoil;

    [Tooltip("Enables/Disables the code that plays the shoot animation. Note: Only select this if your gun's animator has a state called shoot.")]
    public bool hasShootAnimation;

    [Header("UI Configuration")]

    [Tooltip("The amount each piece of the crosshair is moved when one shot is fired.")]
    public float crosshairDisplaceAmount;

    [Header("Audio Configuration")]

    public GameObject shootSound;
    public GameObject reloadSound;
    public GameObject newBulletSound;

}
