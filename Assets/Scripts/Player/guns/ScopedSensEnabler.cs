using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ScopedSensEnabler : MonoBehaviour {

    public bool scoped;
    public GunInfo gunInfo;
    public Look look;

    void Update() {
        if (scoped && gunInfo.sniperADS) {
            look.scoped = true;
        }

        else {
            look.scoped = false;
        }
    }

}
