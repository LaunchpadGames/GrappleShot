using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class Viewmodel : MonoBehaviour
{
    public GameObject viewmodel;
    public GameObject worldmodelItems;
    public PhotonView PV;

    void Awake() {
        if (PV.IsMine){
            worldmodelItems.SetActive(false);
            viewmodel.SetActive(true);
        }
        else{
            worldmodelItems.SetActive(true);
            viewmodel.SetActive(false);
        }
    }
}