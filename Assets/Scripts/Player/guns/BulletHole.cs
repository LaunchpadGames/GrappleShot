using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletHole : MonoBehaviour
{
    public Material bulletHoleHot;
    public Material bulletHoleCold;
    public float coolSpeed;
    private Material bulletHoleCurrent;
    public MeshRenderer mesh;
    // Start is called before the first frame update
    void Start()
    {
        bulletHoleCurrent = new Material(bulletHoleHot);
        mesh.material = bulletHoleCurrent;
    }

    // Update is called once per frame
    void Update()
    {
        bulletHoleCurrent.Lerp(bulletHoleCurrent, bulletHoleCold, Time.deltaTime * coolSpeed);
    }
}
