using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;

public class UsernameDisplay : MonoBehaviour
{
    [SerializeField] PhotonView playerPV;
    [SerializeField] TMP_Text usernameText;

    void Start() {
        if (playerPV.IsMine){
            gameObject.SetActive(false);
        }
        usernameText.text = playerPV.Owner.NickName;
    }
}
