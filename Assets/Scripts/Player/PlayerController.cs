using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using Unity.VisualScripting;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using TMPro;
using UnityEngine.UI;

public class PlayerController : MonoBehaviourPunCallbacks, IDamageable
{
    [SerializeField] GameObject cameraHolder, ui, camera, viewmodelCam, sniperScopeCam, minimapCam;
    [SerializeField] Item[] items;
    [SerializeField] TMP_Text playerNameText, weaponNameText;
    [SerializeField] Image healthBarImage, weaponImage;
    [SerializeField] Animator topHalfAnimator;
    
    int itemIndex;
    int previousItemIndex = -1;
    
    Rigidbody rb;
    PhotonView PV;
    [SerializeField] Camera cam;
    public PlayerMovement playerMovement;
    
    const float maxHealth = 100;
    float currentHealth;
    public bool canSwitch = true;
    
    PlayerManager playerManager;
    public TextMeshProUGUI ammoCounter;
    [Header("Enviroment")]
    public float updraftForce;

    void Awake() {
        rb = GetComponent<Rigidbody>();
        PV = GetComponent<PhotonView>();
        
        playerManager = PhotonView.Find((int)PV.InstantiationData[0]).GetComponent<PlayerManager>();
    }

    void Start() {
        if (PV.IsMine){
            currentHealth = maxHealth;
            EquipItem(0);
            cam.fieldOfView = PlayerPrefs.GetFloat("fov") / ((float)cam.pixelWidth / cam.pixelHeight);
        }
        else{
            Destroy(GetComponentInChildren<Camera>().gameObject);
            Destroy(rb);
            Destroy(ui);
            Destroy(camera);
            Destroy(sniperScopeCam);
            Destroy(viewmodelCam);
            Destroy(minimapCam);
            playerMovement.enabled = false;
            gameObject.layer = 7;
            SetLayerAllChildren(gameObject.transform,7);
        }

        playerNameText.text = PhotonNetwork.NickName;
    }
    

    void Update() {
        if (!PV.IsMine)
            return;

        for(int i = 0; i < items.Length; i++){
            if (Input.GetKeyDown((i + 1).ToString())){
                // StartCoroutine(Switch());
                EquipItem(i);
                break;
            }
        }

        if (Input.GetAxisRaw("Mouse ScrollWheel") > 0f){
            if(itemIndex >= items.Length - 1){
                // StartCoroutine(Switch());
                EquipItem(0);
            }
            else{
                // StartCoroutine(Switch());
                EquipItem(itemIndex + 1);
            }
        }
        else if(Input.GetAxisRaw("Mouse ScrollWheel") < 0f){
            if(itemIndex <= 0){
                // StartCoroutine(Switch());
                EquipItem(items.Length - 1);
            }
            else{
                // StartCoroutine(Switch());
                EquipItem(itemIndex - 1);
            }
        }
        
        if(((GunInfo)items[itemIndex].itemInfo).isAutomatic)
        {
            if (Input.GetMouseButton(0))
            {
                items[itemIndex].Use();
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                items[itemIndex].Use();
            }
        }
        
        if (transform.position.y < -10f){
            Die();
        }
    }

    // IEnumerator Switch() {
    //     viewmodelAnimator.SetBool("switching", true);
    //     yield return new WaitForSeconds(0.33f);
    //     viewmodelAnimator.SetBool("switching", false);
    // }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Updraft Zone")
        {
            rb.AddForce(transform.up * updraftForce, ForceMode.Force);
        }
    }
    
    //what the fuck??
    void EquipItem(int _index) {
        if (!canSwitch)
            return;

        if(_index == previousItemIndex)
            return;
        
        itemIndex = _index;

        items[itemIndex].itemGameObject.SetActive(true);
        items[itemIndex].itemGameObject.transform.parent.gameObject.GetComponent<GunController>().crosshair.SetActive(true);


        if (previousItemIndex != -1){
            
            items[previousItemIndex].itemGameObject.SetActive(false);
            items[previousItemIndex].itemGameObject.transform.parent.gameObject.GetComponent<GunController>().crosshair.SetActive(false);
        }
        
        previousItemIndex = itemIndex;

        weaponNameText.text = items[itemIndex].itemInfo.itemName;
        weaponImage.sprite = items[itemIndex].itemInfo.itemImage;
        ammoCounter.text = items[itemIndex].itemGameObject.transform.parent.GetComponent<GunController>().currentAmmo.ToString();


        if (PV.IsMine){
            topHalfAnimator.SetInteger("holdingItemIndex", itemIndex);
            Hashtable hash = new Hashtable();
            hash.Add("itemIndex", itemIndex);
            PhotonNetwork.LocalPlayer.SetCustomProperties(hash);
        }
    }
    
    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps) {
        if (changedProps.ContainsKey("itemIndex") && !PV.IsMine && targetPlayer == PV.Owner){
            EquipItem((int) changedProps["itemIndex"]);
        }
    }

    public void TakeDamage(float damage) {
        PV.RPC(nameof(RPC_TakeDamage), PV.Owner, damage);
    }
    
    [PunRPC]
    void RPC_TakeDamage(float damage, PhotonMessageInfo info) {
        currentHealth -= damage;
        
        healthBarImage.fillAmount = currentHealth / maxHealth;
        
        if (currentHealth <= 0){
            Die();
            PlayerManager.Find(info.Sender).GetKill();
        }
    }

    void Die() {
        playerManager.Die();
    }
    
    void SetLayerAllChildren(Transform root, int layer)
    {
        var children = root.GetComponentsInChildren<Transform>(includeInactive: true);
        foreach (var child in children)
        {
            child.gameObject.layer = layer;
        }
    }

}
