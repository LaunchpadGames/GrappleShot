using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Look : MonoBehaviour
{
    [Header("References")]
    [SerializeField] Wallrun wallrun;
    [SerializeField] PlayerMovement playerMovement;

    [SerializeField] private float sens = 1f;
    [SerializeField] private float scopedSens = 0.75f;
    public bool scoped = false;

    [SerializeField] Transform cam;
    [SerializeField] Transform worldModel;
    [SerializeField] Transform worldModelHead;
    [SerializeField] Transform orientation;
    [SerializeField] Transform minimap;

    float mouseX;
    float mouseY;

    float xRotation;
    float yRotation;

    private void Start() {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
        sens = PlayerPrefs.GetFloat("sens");
        scopedSens = PlayerPrefs.GetFloat("scoped sens");
        
        mouseX = Input.GetAxisRaw("Mouse X");
        mouseY = Input.GetAxisRaw("Mouse Y");

        if (scoped){
            yRotation += mouseX * scopedSens;
            xRotation -= mouseY * scopedSens;
        }
        else{
            yRotation += mouseX * sens;
            xRotation -= mouseY * sens;
        }
        
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        float sumOfTilt = playerMovement.tilt + wallrun.tilt;
        float averageTilt = sumOfTilt / 2;
        
        cam.transform.rotation = Quaternion.Euler(xRotation, yRotation, averageTilt);
        worldModel.transform.rotation = Quaternion.Euler(worldModel.transform.rotation.x, yRotation, wallrun.tilt);
        minimap.transform.rotation = Quaternion.Euler(minimap.transform.rotation.x, yRotation, minimap.transform.rotation.x);
        worldModelHead.transform.rotation = Quaternion.Euler(xRotation, yRotation, worldModelHead.transform.rotation.z);
        orientation.transform.rotation = Quaternion.Euler(0, yRotation, 0);
    }
}