using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuRotate : MonoBehaviour
{
    public float rotationSpeed = 0.02f;
    
    void Update() {
        transform.Rotate(0, rotationSpeed, 0 * Time.deltaTime);
    }
}