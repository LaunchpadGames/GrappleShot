using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class MinimapPlayerIndicator : MonoBehaviour
{
    [SerializeField] Image image;
    [SerializeField] PhotonView PV;
    [SerializeField] Sprite[] indicators;

    public void Awake() {
        if (PV.IsMine) {
            image.sprite = indicators[0];
        }
        else {
            image.sprite = indicators[2];
        }
    }

}
