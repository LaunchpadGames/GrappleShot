using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuSceneLoader : MonoBehaviour
{
    public String[] scenes;

    void Awake() {
        var randomIndex = UnityEngine.Random.Range(0, scenes.Length);
    
        SceneManager.LoadScene(scenes[randomIndex]);
        SceneManager.LoadScene("MenuUI", LoadSceneMode.Additive);
    }
}
