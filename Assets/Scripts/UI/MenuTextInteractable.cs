using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class MenuTextInteractable : MonoBehaviour
{
    [SerializeField] Button button;
    [SerializeField] TMP_Text text;
    void Update()
    {
        if (button.interactable){
            text.faceColor = new Color32(255, 255, 255, 255);
        }
        else{
            text.faceColor = new Color32(255, 255, 255, 80);
        }
    }
}
