using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class VersionUpdater : MonoBehaviour
{
    [SerializeField] string versionType;
    public TextMeshProUGUI textDisplay;

    void Start()
    {
        textDisplay.text = versionType + " " + Application.version;
    }
}
