using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using TMPro;
using UnityEditor;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    [SerializeField] AudioMixer audioMixer;
    [SerializeField] TMP_Dropdown qualityDropdown,resolutionDropdown,windowDropdown;
    [SerializeField] Slider fovSlider,sensSlider, scopedSensSlider,masterSlider, musicSlider, sfxSlider,ambientSlider,voiceLinesSlider;
    [SerializeField] Toggle fpsToggle, pingToggle;
    [SerializeField] TMP_Text fovText, sensText, scopedSensText;
    
    Resolution[] resolutions;

    void Start() {
        if (PlayerPrefs.GetInt("quality") != null){
            qualityDropdown.value = PlayerPrefs.GetInt("quality");
        }
        else{
            qualityDropdown.value = QualitySettings.GetQualityLevel();
        }

        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();
        
        List<string> options = new List<string>();
        int currentResolutionIndex = 0;
        for (int i = 0; i < resolutions.Length; i++){
            string option = resolutions[i].width + "x" + resolutions[i].height;
            
            options.Add(option);
            
            if(resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height){
                currentResolutionIndex = i;
            }
        }
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();

        
        //read playerprefs for volume
        if (PlayerPrefs.HasKey("master")){
            audioMixer.SetFloat("master", PlayerPrefs.GetFloat("master"));
            masterSlider.value = PlayerPrefs.GetFloat("master");
        }
        if (PlayerPrefs.HasKey("music")){
            audioMixer.SetFloat("music", PlayerPrefs.GetFloat("music"));
            musicSlider.value = PlayerPrefs.GetFloat("music");
        }
        if (PlayerPrefs.HasKey("sfx")){
            audioMixer.SetFloat("sfx", PlayerPrefs.GetFloat("sfx"));
            sfxSlider.value = PlayerPrefs.GetFloat("sfx");
        }
        if (PlayerPrefs.HasKey("ambient")){
            audioMixer.SetFloat("ambient", PlayerPrefs.GetFloat("ambient"));
            sfxSlider.value = PlayerPrefs.GetFloat("ambient");
        }
        if (PlayerPrefs.HasKey("voice lines")){
            audioMixer.SetFloat("voice lines", PlayerPrefs.GetFloat("voice lines"));
            sfxSlider.value = PlayerPrefs.GetFloat("voice lines");
        }
        
        //read playerprefs for quality
        if (PlayerPrefs.HasKey("quality")){
            QualitySettings.SetQualityLevel(PlayerPrefs.GetInt("quality"));
            qualityDropdown.value = PlayerPrefs.GetInt("quality");
        }
        
        //read playerprefs for resolution and fullscreen
        if (PlayerPrefs.HasKey("screenType")){
            if (PlayerPrefs.GetInt("screenType") == 0){
                Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
                windowDropdown.value = PlayerPrefs.GetInt("screenType");
            }
            else if (PlayerPrefs.GetInt("screenType") == 1){
                Screen.fullScreenMode = FullScreenMode.FullScreenWindow;
                windowDropdown.value = PlayerPrefs.GetInt("screenType");
            }
            else{
                Screen.fullScreenMode = FullScreenMode.Windowed;
                windowDropdown.value = PlayerPrefs.GetInt("screenType");
            }
        }

        if (PlayerPrefs.HasKey("resolution")){
            Resolution resolution = resolutions[PlayerPrefs.GetInt("resolution")];
            Screen.SetResolution(resolutions[PlayerPrefs.GetInt("resolution")].width, resolutions[PlayerPrefs.GetInt("resolution")].height, Screen.fullScreenMode);
        }
        
        //fps and ping
        if (PlayerPrefs.HasKey("fps")){
            fpsToggle.isOn = PlayerPrefs.GetInt("fps") == 1 ? true : false;
        }
        if (PlayerPrefs.HasKey("ping")){
            pingToggle.isOn = PlayerPrefs.GetInt("ping") == 1 ? true : false;
        }
        
        //fov
        if (PlayerPrefs.HasKey("fov")){
            float fov = PlayerPrefs.GetFloat("fov");
            float fovIndex = fov - 55;
            fovIndex = fovIndex / 5;
            fovSlider.value = fovIndex;
            fovText.text = "FOV ("+fov+")";
        }
        else{
            PlayerPrefs.SetFloat("fov", 105);
            fovSlider.value = 10;
            fovText.text = "FOV ("+105+")";
        }
        
        //sens and scoped sens
        if (PlayerPrefs.HasKey("sens")){
            sensSlider.value = PlayerPrefs.GetFloat("sens");
            sensText.text = "Sensitivity ("+PlayerPrefs.GetFloat("sens")+")";
        }
        else{
            PlayerPrefs.SetFloat("sens", 1f);
            sensSlider.value = 1f;
            sensText.text = "Sensitivity (1)";
        }
        
        if (PlayerPrefs.HasKey("scoped sens")){
            scopedSensSlider.value = PlayerPrefs.GetFloat("scoped sens");
            scopedSensText.text = "Scoped Sensitivity ("+PlayerPrefs.GetFloat("scoped sens")+")";
        }
        else{
            PlayerPrefs.SetFloat("scoped sens", 0.75f);
            scopedSensSlider.value = 0.75f;
            scopedSensText.text = "Scoped Sensitivity (0.75)";
        }
    }

    public void SetVolumeMaster(float volume) {
        audioMixer.SetFloat("master", volume);
        PlayerPrefs.SetFloat("master", volume);
        PlayerPrefs.Save();
    }
    
    public void SetVolumeMusic(float volume) {
        audioMixer.SetFloat("music", volume);
        PlayerPrefs.SetFloat("music", volume);
        PlayerPrefs.Save();
    }
    
    public void SetVolumeSFX(float volume) {
        audioMixer.SetFloat("sfx", volume);
        PlayerPrefs.SetFloat("sfx", volume);
        PlayerPrefs.Save();
    }
    
    public void SetVolumeAmbient(float volume) {
        audioMixer.SetFloat("ambient", volume);
        PlayerPrefs.SetFloat("ambient", volume);
        PlayerPrefs.Save();
    }
    
    public void SetVolumeVoiceLines(float volume) {
        audioMixer.SetFloat("voice lines", volume);
        PlayerPrefs.SetFloat("voice lines", volume);
        PlayerPrefs.Save();
    }
    
    public void SetQuality(int qualityIndex) {
        QualitySettings.SetQualityLevel(qualityIndex);
        PlayerPrefs.SetInt("quality", qualityIndex);
        PlayerPrefs.Save();
    }
    
    public void SetFullscreen(int screenType) {
        if (screenType == 0){
            Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
        }
        else if (screenType == 1){
            Screen.fullScreenMode = FullScreenMode.FullScreenWindow;
        }
        else{
            Screen.fullScreenMode = FullScreenMode.Windowed;
        }
        
        PlayerPrefs.SetInt("screenType", screenType);
        PlayerPrefs.Save();
    }
    
    public void SetResolution(int resolutionIndex) {
        Resolution resolution = resolutions[resolutionIndex];
        PlayerPrefs.SetInt("resolution", resolutionIndex);
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreenMode);
        PlayerPrefs.Save();
    }

    public void SetFPSOverlay(bool enabled) {
        PlayerPrefs.SetInt("fps", enabled ? 1 : 0);
        PlayerPrefs.Save();
    }
    
    public void SetPingOverlay(bool enabled) {
        PlayerPrefs.SetInt("ping", enabled ? 1 : 0);
        PlayerPrefs.Save();
    }
    
    public void SetFOV(float fov) {
        fov = fov * 5 + 55;
        PlayerPrefs.SetFloat("fov", fov);
        fovText.text = "FOV ("+fov.ToString()+")";
        Debug.Log(fov);
        PlayerPrefs.Save();
    }
    
    public void SetSens(float sens) {
        sens = (float) Math.Round(sens, 2);
        PlayerPrefs.SetFloat("sens", sens);
        sensText.text = "Sensitivity ("+sens.ToString()+")";
        PlayerPrefs.Save();
    }
    
    public void SetScopedSens(float scopedSens) {
        scopedSens = (float) Math.Round(scopedSens, 2);
        PlayerPrefs.SetFloat("scoped sens", scopedSens);
        scopedSensText.text = "Scoped Sensitivity ("+scopedSens.ToString()+")";
        PlayerPrefs.Save();
    }
}
