using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingMenu : MonoBehaviour
{
    public PhotonView PV;
    
    public string[] MapNames;
    public Sprite[] MapImage;
    public string[] MapNamesHuman;
    public string[] MapDetails;
    
    public Image MapImageUI;
    public TMP_Text MapNameUI;
    public TMP_Text MapDetailsUI;

    [HideInInspector] public int sceneIndex;

    public void Start() {
        
        if (Launcher.mapSelection == "Test Scene"){
            sceneIndex = 0;
        }
        else if (Launcher.mapSelection == "Rooftops"){
            sceneIndex = 1;
        }
        else{
            sceneIndex = 0;
        }
        
        int detailsIndex = Random.Range(0, MapDetails.Length);
            
        MapImageUI.sprite = MapImage[sceneIndex]; 
        MapNameUI.text = MapNamesHuman[sceneIndex];
        MapDetailsUI.text = MapDetails[detailsIndex];
            
        PhotonNetwork.LoadLevel(MapNames[sceneIndex]);
    }

}
