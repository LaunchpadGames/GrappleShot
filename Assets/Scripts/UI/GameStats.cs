using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;

public class GameStats : MonoBehaviour {

    [SerializeField] GameObject fpsObject, pingObject;
    [SerializeField] TMP_Text fpsText, pingText;
    
    private void Awake() {
        StartCoroutine(FPS());
        StartCoroutine(Ping());
    }

    private IEnumerator FPS() {
        while (true) {
            fpsText.text = "FPS: " + (int)(1.0f / Time.deltaTime);
            yield return new WaitForSeconds(0.1f);
        }
    }
    
    private IEnumerator Ping() {
        while (true) {
            pingText.text = "Ping: " + PhotonNetwork.GetPing() +" ms";
            yield return new WaitForSeconds(0.1f);
        }
    }

    void Update() {
        if (PlayerPrefs.GetInt("fps") == 1) {
            fpsObject.SetActive(true);
        }
        else {
            fpsObject.SetActive(false);
        }
        
        if (PlayerPrefs.GetInt("ping") == 1) {
            pingObject.SetActive(true);
        }
        else {
            pingObject.SetActive(false);
        }
    }

}
