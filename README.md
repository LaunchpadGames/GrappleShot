# GrappleShot

This project has been paused indefinitely. To be honest, I haven't worked on it in a very long time for a variety of reasons.

GrappleShot has been uploaded here sort of as an archival project.

The main branch contains the second attempt for this project (Started October 2022), while the branch titled "old" contains the original project (Started August 2020).

If you do want to build and play this for some reason, you will need to provide your own PhotonEngine app id in the file located at `Assets/Photon/PhotonUnityNetworking/Resources/PhotonServerSettings.asset`, to access the multiplayer. You can create one for free on the [Photon website](https://dashboard.photonengine.com/). I've removed my own app id so people won't be able to hijack my servers in case i ever want to use it again. _Multiplayer on previous builds of GrappleShot will remain operational._

For more support, join the [Discord server](https://discord.launchpad-games.com)!

[GrappleShot](https://maxb0tbeep.itch.io/grappleshot) by [Launchpad Games](https://www.launchpad-games.com) is licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0)

![CC BY-NC-SA-4.0](https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-nc-sa.png)
